import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from './Movie';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }

  loadMovies(): Observable<Movie> {
    return this.http.get<Movie>('https://frozen-reef-76341.herokuapp.com/viewMovies')
  }

  addMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>('https://frozen-reef-76341.herokuapp.com/addMovie', movie)
  }

  searchMovie(genre: string): Observable<Movie> {
    return this.http.get<Movie>('https://frozen-reef-76341.herokuapp.com/searchMovie?genre=' + genre)
  }
}
