import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-search-movies',
  templateUrl: './search-movies.component.html',
  styleUrls: ['./search-movies.component.css']
})
export class SearchMoviesComponent implements OnInit {

  movieGenre: string = null
  searched: boolean = false
  movies: any = []

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
  }

  search(searchForm: NgForm) {
    if (searchForm.valid) {
      this.movieService.searchMovie(this.movieGenre).subscribe(
        res => {
          this.searched = true
          this.movies = res
        }
      )
    }
  }

}
