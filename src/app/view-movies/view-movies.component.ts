import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-view-movies',
  templateUrl: './view-movies.component.html',
  styleUrls: ['./view-movies.component.css']
})
export class ViewMoviesComponent implements OnInit {

  movies: any = []

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.getMovies()
  }

  getMovies() {
    this.movieService.loadMovies().subscribe(
      res => {
        this.movies = res
      }
    )
  }

}
