export class Movie {
    movieName: string
    movieRating: number
    movieGenre: string
}