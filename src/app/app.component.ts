import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  addClass: string = 'nav-item nav-link'
  viewClass: string = 'nav-item nav-link active'
  searchClass: string = 'nav-item nav-link'

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  toggleView() {
    this.viewClass = 'nav-item nav-link active'
    this.searchClass = 'nav-item nav-link'
    this.addClass = 'nav-item nav-link'
  }

  toggleAdd() {
    this.viewClass = 'nav-item nav-link '
    this.searchClass = 'nav-item nav-link'
    this.addClass = 'nav-item nav-link active'
  }

  toggleSearch() {
    this.viewClass = 'nav-item nav-link'
    this.searchClass = 'nav-item nav-link active'
    this.addClass = 'nav-item nav-link'
  }

}
