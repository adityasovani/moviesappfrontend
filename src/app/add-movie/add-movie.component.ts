import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MovieService } from '../movie.service';
import { Movie } from '../Movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  movieName: string = null
  movieRating: number = null
  movieGenre: string = null
  response: string = null

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
  }

  addMovie(form: NgForm) {
    if (form.valid) {
      let movie = new Movie()
      movie.movieName = this.movieName
      movie.movieRating = this.movieRating
      movie.movieGenre = this.movieGenre
      this.movieService.addMovie(movie).subscribe(
        res => {
          this.response = res.movieName
        }
      )
    }
  }
}
