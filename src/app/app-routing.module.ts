import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewMoviesComponent } from './view-movies/view-movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { SearchMoviesComponent } from './search-movies/search-movies.component';


const routes: Routes = [
  { path: '', component: ViewMoviesComponent },
  { path: 'add', component: AddMovieComponent },
  { path: 'search', component: SearchMoviesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
